package com.j30.collections.array;

public class Main {
    public static void main(String[] args) {
        MyArrayList list = new MyArrayList();

        list.add("abc");
        list.add("def");
        list.add(1);
        list.add(5.9);
        list.add(0.3);
        list.add(0.4);
        list.add(0.5);
        list.add(0.6);
        list.add(0.7); // to jest 8 element
        list.add(0.8);
        list.add(0.9);
        list.add(0.10);
        list.add(0.11);
        list.add(0.12);


        System.out.println(list);
        list.remove(8);                 //0.7
        System.out.println(list);
        list.remove(8);                 //0.8
        System.out.println(list);
        list.remove(8);                 //0.9
        System.out.println(list);
        list.remove(0);                 //usuń pierwszy
        System.out.println(list);
        list.remove(list.size() - 1);   //usuń ostatni
        System.out.println(list);
    }
}
